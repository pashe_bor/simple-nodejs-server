const express = require('express'),
    router = express.Router();
const User = require('./../models/User');


router.get('/users', function (request, response) {
    User.find(function (error, users) {
        if (error) return handleError(error).send(error);
        response.send(users);
    })
});

module.exports = router;