const express = require('express'),
    router = express.Router();
const Movies = require('./../models/Movies');
const assert = require('assert');


router.get('/movies', function (request, response) {
    Movies.find(function(error, users) {
        if (error) return handleError(error).send(error);
        response.send(users);
    });
});

router.put('/movies', function (request, response) {
    let data = request.body;
    let movie = new Movies(data);
    movie.save(function (error, docs) {
        if (error) return handleError(error).send(error);
        assert.notEqual(docs['name'], false);
        assert.notEqual(docs['test'], false);
        response.send(data);
    });
});

router.delete('/movies/:id', function (request, response) {
    let id = request.params.id;
    Movies.findByIdAndRemove(id, function (error, movie) {
        if (error) return response.status(500).send(error);
        const res = {
            message: "Movie " + movie.name + " successfully deleted",
            id: movie._id
        };
        response.send(res);
    })
});

router.post('/movies', function (request, response) {
    let user = request.body;
    Movies.update({_id: user._id},  user, function (error, docs) {
        if (error) return response.status(500).send(error);
        response.send(docs);
    })
});

module.exports = router;
