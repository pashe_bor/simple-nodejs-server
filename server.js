var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', require('./routes/movies'));
app.use('/', require('./routes/users'));

app.get('/graphs', function(req, res) {
    var json = JSON.parse(fs.readFileSync('./data/graph.json', 'utf8'));
    res.send(json);
});


app.listen(3000, function () {
    console.log('Test app is listening on port 8080!');
});