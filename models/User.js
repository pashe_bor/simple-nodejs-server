const mongoose = require('mongoose');
const options = {
    user: 'pashebor',
    pass: 'ltvmzyjd90',
    autoIndex: false, // Don't build indexes
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0
};

const userConnect = mongoose.createConnection('mongodb://localhost:27017/admin?authSource=admin', options);

const users = new mongoose.Schema({
    user: String,
    pswd: String
}, {collection: 'system.users'});

const User = userConnect.model('User', users);

module.exports = User;